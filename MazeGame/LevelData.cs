﻿namespace MazeGame
{
    public class LevelData{
        public GameMap Map;
        public int Time;
        public LevelData(GameMap map, int time) {
            Map = map; Time = time;
        }
    }
}
