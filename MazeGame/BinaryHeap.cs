﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    /// <summary>
    /// Creates a stable binary heap of objects T, ordered by a priority value K
    /// </summary>
    /// <typeparam name="T">Type of objects to be stored in the heap</typeparam>
    /// <typeparam name="K">Type of keys to be used for prioritizing</typeparam>
    public class BinaryHeap<T, K> : IComparer<Tuple<T, K, int>> where K : IComparable
    {
        /// <summary>
        /// is increased each time an object is added to the heap. this allows the heap to be stable.
        /// </summary>
        public Int32 counter
        {
            get;
            private set;
        }

        /// <summary>
        /// The heap can be a minHeap, with the minimum element being at the root of the tree, or a maxHeap, with the maximum element at the root.
        /// </summary>
        public enum HeapType
        {
            MinHeap,
            MaxHeap
        }

        /// <summary>
        /// An list for the internal storage of items.
        /// </summary>
        List<Tuple<T, K, int>> items;

        /// <summary>
        /// The heap can be a minHeap, with the minimum element being at the root of the tree, or a maxHeap, with the maximum element at the root.
        /// </summary>
        public HeapType type { get; private set; }

        public BinaryHeap(HeapType type)
        {
            this.items = new List<Tuple<T, K, int>>() { default(Tuple<T, K, int>) };
            this.type = type;
            this.counter = 0;
        }

        /// <summary>
        /// Extracts the top element (the minimum in a minHeap, the maximum in a MaxHeap), and reorders the children
        /// to maintain the heap property.
        /// </summary>
        /// <returns>The root element</returns>
        public T ExtractTop()
        {
            items.Swap(1, items.Count - 1);
            T min = items.Last().Item1;
            items.RemoveAt(items.Count - 1);
            Heapify(1);
            return min;
        }

        /// <summary>
        /// Compares the 2 items by priority. If the priority is the same, precedence is given to the item added Item1.
        /// </summary>
        public int Compare(Tuple<T, K, int> a, Tuple<T, K, int> b)
        {
            if (a.Item2.CompareTo(b.Item2) != 0)
                return a.Item2.CompareTo(b.Item2);
            else
                return a.Item3.CompareTo(b.Item3);
        }

        /// <summary>
        /// Increases the counter with each item added. Used for stability.
        /// </summary>
        void updateCounter()
        {
            switch (type)
            {
                case HeapType.MinHeap:
                    ++counter;
                    break;
                case HeapType.MaxHeap:
                    --counter;
                    break;
            }
        }

        /// <summary>
        /// Adds a new item to the heap and reorderes the elements to restore the heap property.
        /// </summary>
        /// <param name="item">Item to be added.</param>
        /// <param name="key">Priority of the item.</param>
        public void Add(T item, K key)
        {
            Tuple<T, K, int> entry = new Tuple<T, K, int>(item, key, this.counter);
            this.items.Add(entry);
            updateCounter();
            for (int i = (items.Count - 1) / 2; i != 0; i--)
            {
                Heapify(i);
            }
        }

        /// <summary>
        /// Returns the root item without deleting it from the heap.
        /// </summary>
        /// <returns>The root item</returns>
        public T PeekTop()
        {
            return items[1].Item1;
        }

        int leftChild(int i)
        {
            return 2 * i;
        }

        int rightChild(int i)
        {
            return 2 * i + 1;
        }

        int parent(int i)
        {
            return i / 2;
        }

        /// <summary>
        /// Returns the number of items currently stored in the heap.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            //it's items.Count -1, because items[0] is just a default object.
            return items.Count - 1;
        }

        /// <summary>
        /// Returns true if the heap contains the given item.
        /// False if the item is not found in the heap.
        /// </summary>
        public bool Contains(T item)
        {
            bool contains = false;
            foreach (Tuple<T, K, int> pair in items)
            {
                if (pair.Item1.Equals(item))
                    contains = true;
            }
            return contains;
        }

        /// <summary>
        /// Returns true if there are no items stored in the heap.
        /// Returns false if there is at least one item.
        /// </summary>
        public bool isEmpty
        {
            get
            {
                bool empty = true;
                if (items.Count - 1 > 0)
                    empty = false;
                return empty;
            }
        }

        /// <summary>
        /// If the left and right children of the item stored @ index have the heap property,
        /// compares the children with the index node, swaps parent with one of them if needed, then recursively 
        /// heapifies the swapped children
        /// </summary>
        /// <param name="index"></param>
        void Heapify(int index)
        {
            int swapChild = index;
            if (index >= items.Count) return;
            bool compareLeft = (leftChild(index) < items.Count) ? true : false;
            bool compareRight = (rightChild(index) < items.Count) ? true : false;

            // if we're big enough to have a right child, we're big enough to have a left one, too!
            if (compareRight)
            {
                //if left child is smaller
                if (Compare(items[leftChild(index)], items[rightChild(index)]) < 0)
                {
                    switch (this.type)
                    {
                        case HeapType.MaxHeap:
                            if (Compare(items[index], items[rightChild(index)]) < 0)
                                swapChild = rightChild(index);
                            break;
                        case HeapType.MinHeap:
                            if (Compare(items[index], items[leftChild(index)]) > 0)
                                swapChild = leftChild(index);
                            break;
                    }
                }
                else if (Compare(items[leftChild(index)], (items[rightChild(index)])) > 0)
                {
                    switch (this.type)
                    {
                        case HeapType.MaxHeap:
                            if (Compare(items[index], items[leftChild(index)]) < 0)
                                swapChild = leftChild(index);
                            break;
                        case HeapType.MinHeap:
                            if (Compare(items[index], items[rightChild(index)]) > 0)
                                swapChild = rightChild(index);
                            break;
                    }
                }
            }
            else if (compareLeft)
            {
                if (Compare(items[index], items[leftChild(index)]) < 0)
                {
                    if (this.type == HeapType.MaxHeap)
                        swapChild = leftChild(index);
                }
                else if (Compare(items[index], items[leftChild(index)]) != 0)
                {
                    if (this.type == HeapType.MinHeap)
                        swapChild = leftChild(index);
                }

            }
            if (index != swapChild)
            {
                items.Swap(index, swapChild);
                Heapify(swapChild);
            }
        }
 
    }
}
