﻿namespace MazeGame
{
    public class EnemyE : Roamer
    {
        public EnemyE(Vector2 position) : base("E", position, MovementSequences.SeqE, ObjectType.EnemyE) { }


        public override Vector2 NextDirection()
        {
            return Instructions[counter % 8];
        }
    }
}
