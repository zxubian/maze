﻿namespace MazeGame
{
    public class EnemyB : Chaser
    {
        public EnemyB(Vector2 position) : base("B", position,MovementSequences.SeqB, ObjectType.EnemyB) {}
        public override Move NextMove()
        {
            Move nextMove = new Move();
            nextMove.Start = Position;
            nextMove.MovingObject = this;
            UpdateDeltaPos();
            int moveX = ShouldMoveX();
            if (moveX != 0)
            {
                nextMove.AbsoluteDirection = Vector2.Right.Times(moveX);
            }
            else
            {
                int moveY = ShouldMoveY();
                if (moveY != 0)
                {
                    nextMove.AbsoluteDirection = Vector2.Up.Times(moveY);
                }
                else
                {
                    nextMove.AbsoluteDirection = FirstPossibleDirection();
                }
            }
            nextMove.End = Vector2.Add(nextMove.Start, nextMove.AbsoluteDirection);
            return nextMove;
        }
    }
}

