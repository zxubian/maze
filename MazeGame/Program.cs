﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    class Program
    {
        static int Main(string[] args)
        {
            Loader loader = new Loader();
            LevelData data = loader.LoadLevelData(args[0]);
            GameSimulator simulator = new GameSimulator();
            simulator.StartSimulation(data);
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            return 0;
        }
    }
}
