﻿namespace MazeGame
{
    public class EnemyA: Chaser
    {
        public EnemyA(Vector2 position) : base("A", position, MovementSequences.SeqA, ObjectType.EnemyA) { }

        public override Move NextMove()
        {
            Move nextMove = new Move();
            nextMove.Start = Position;
            nextMove.MovingObject = this;
            UpdateDeltaPos();
            int moveY = ShouldMoveY();
            if (moveY != 0)
            {
                nextMove.AbsoluteDirection = Vector2.Up.Times(moveY);
            }
            else
            {
                int moveX = ShouldMoveX();
                if (moveX != 0)
                {
                    nextMove.AbsoluteDirection = Vector2.Right.Times(moveX);
                }
                else
                {
                    nextMove.AbsoluteDirection = FirstPossibleDirection();
                }
            }
            nextMove.End = Vector2.Add(nextMove.Start, nextMove.AbsoluteDirection);
            return nextMove;
        }
    }
}
