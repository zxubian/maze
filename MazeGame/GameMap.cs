﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class GameMap
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Tile[,] TileArray { get; private set; }
        public Dictionary<String,Item> Items { get; private set; }
        public Dictionary<String,Tile> ItemTiles { get; private set; }
        public List<GameObject> Enemies { get; private set; }
        public Player PlayerObj { get; private set; }
        public GameObject Goal { get; private set; }
        public static GameMap Instance { get; private set; }

        //constructor
        public GameMap(string[] mapString)
        {
            Vector2 MapDimensions = GetMapDimensions(ref mapString);
            InitializeTileArray(MapDimensions);
            InitializeLists();
            PopulateArrays(ref mapString);
            InitializeNeighbors();
            Instance = this;
        }

        public void InitializeNeighbors() {
            for (int j = 0; j != Height; ++j) {
                for (int i = 0; i != Width; ++i)
                {
                    Tile currentTile = TileArray[i, j];
                    if (i != 0)
                        currentTile.AddNeighbor(TileArray[i - 1,j]);
                    if (i != Width - 1)
                        currentTile.AddNeighbor(TileArray[i + 1, j]);
                    if (j != 0)
                        currentTile.AddNeighbor(TileArray[i, j - 1]);
                    if (j != Height - 1)
                        currentTile.AddNeighbor(TileArray[i, j + 1]);
                }
            }
        }

        public Vector2 GetMapDimensions(ref string[] mapString) {
            int MapHeight = mapString.Length;
            int MapWidth = mapString[0].Length;
            return new Vector2(MapWidth, MapHeight);
        }


        void PopulateArrays(ref string[] mapString) {
            GameObjectFactory factory = new GameObjectFactory();
            for(int j = 0; j != Height; ++j){
                for (int i = 0; i != Width; ++i)
                {
                    ObjectType type = Symbol2ObjectType(mapString[j][i]);
                    GameObject obj = factory.Create(type, i, j);
                    TileArray[i, j] = new Tile(new Vector2(i, j));
                    PlaceGameObject(ref obj, i, j);
                    RegisterIfNeeded(ref obj, type);
                }
            }
        }

        void RegisterIfNeeded(ref GameObject gameObject, ObjectType type) {
            switch (type)
            {
                case ObjectType.Player:
                    PlayerObj = (Player)gameObject;
                    break;
                case ObjectType.Goal:
                    Goal = gameObject;
                    break;
                case ObjectType.Item:
                    string coords = gameObject.Position.ToString();
                    Items.Add(coords, gameObject as Item);
                    ItemTiles.Add(coords, FetchTile(gameObject.Position));
                    break;
                case ObjectType.EnemyA:
                case ObjectType.EnemyB:
                case ObjectType.EnemyC:
                case ObjectType.EnemyD:
                case ObjectType.EnemyE:
                    Enemies.Add(gameObject);
                    break;
            }
        }


        void InitializeLists() {
            Enemies = new List<GameObject>();
            Items = new Dictionary<String, Item>();
            ItemTiles = new Dictionary<string, Tile>();
        }

        void InitializeTileArray(Vector2 mapDimensions) {
            Width = mapDimensions.X;
            Height = mapDimensions.Y;
            TileArray = new Tile[Width, Height];
        }


        public bool PlayerOnGoal() {
            if (FetchTile(Player.instance.Position).isGoal)
                return true;
            else return false;
        }

        /// <summary>
        /// Checks if an item is about to be taken on a given move
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public bool CanTakeItem() {
            if (FetchTile(Player.instance.Position).isItem)
                return true;
            else return false;
        }

        /// <summary>
        /// Removes item at current player coordinates
        /// </summary>
        public void PlayerTakeItem() {
            Tile currentItemTile = FetchTile(Player.instance.Position);
            currentItemTile.ToggleFlags(ObjectType.Item, false);
            string coords = currentItemTile.Position.ToString();
            Items.Remove(coords);
            ItemTiles.Remove(coords);
        }

        ObjectType Symbol2ObjectType(char symbol) {
            switch (symbol)
            {
                default:
                    return ObjectType.Empty;
                case '#':
                    return ObjectType.Wall;
                case 'o':
                    return ObjectType.Item;
                case 'G':
                    return ObjectType.Goal;
                case 'A':
                    return ObjectType.EnemyA;
                case 'B':
                    return ObjectType.EnemyB;
                case 'C':
                    return ObjectType.EnemyC;
                case 'D':
                    return ObjectType.EnemyD;
                case 'E':
                    return ObjectType.EnemyE;
                case 'S':
                    return ObjectType.Player;
            }
        }

        public void UpdateWeights(bool shouldCountEnemies) {
            foreach (Tile tile in TileArray) {
                if (tile.isWall)
                    tile.SetWeight(float.PositiveInfinity);
                else if (tile.isEnemy && shouldCountEnemies)
                    tile.SetWeight(float.PositiveInfinity);
                else
                    tile.SetWeight(0f);
            }
        }

        public void PrintGameMap() {
            for (int j = 0; j != Height; ++j) {
                for (int i = 0; i < Width; ++i)
                {
                        Console.Write(TileArray[i, j]);
                }
                Console.Write(Environment.NewLine);
            }
        }

        public void MoveObject(Move move) {
            if (TileUnoccupied(FetchTile(move.End)))
            {
                GameObject movingObject = move.MovingObject;
                movingObject.SetPosition(new Vector2(move.End.X, move.End.Y));
                FetchTile(move.Start).ToggleFlags(movingObject);
                FetchTile(move.End).ToggleFlags(movingObject);
            }
        }

        public bool PlayerDead() {
            if (FetchTile(Player.instance.Position).isEnemy) return true;
            else return false;
        }
                
        public void PlaceGameObject(ref GameObject gameObject, Vector2 position)
        {
            PlaceGameObject(ref gameObject, position.X, position.Y);
        }

        public void PlaceGameObject(ref GameObject gameObject, int x, int y )
        {
            if (gameObject != null)
            {
                TileArray[x, y].ToggleFlags(gameObject);
                gameObject.SetPosition(new Vector2(x, y));
            }
        }

        public bool TileUnoccupied(Vector2 coordinate)
        {
            return TileUnoccupied(FetchTile(coordinate));
        }

        public bool TileUnoccupied(Tile tile)
        {
            if (tile.isWall || tile.isEnemy)
                return false;
            else
                return true;
        }

        public Tile FetchTile(Vector2 coordinates) {
            return TileArray[coordinates.X, coordinates.Y];
        }


    }


    public class GameObjectFactory
    {
        public GameObject Create(ObjectType type, int x, int y) {
            return Create(type, new Vector2(x, y));
        }
        public GameObject Create(ObjectType type, Vector2 position)
        {
            switch (type)
            {
                case ObjectType.Empty:
                    return null;
                case ObjectType.Wall:
                    return new Wall(position);
                case ObjectType.Player:
                    return new Player(position);
                case ObjectType.Goal:
                    return new Goal(position);
                case ObjectType.Item:
                    return new Item(position);
                case ObjectType.EnemyA:
                    return new EnemyA(position);
                case ObjectType.EnemyB:
                    return new EnemyB(position);
                case ObjectType.EnemyC:
                   return new EnemyC(position);
                case ObjectType.EnemyD:
                    return new EnemyD(position);
                case ObjectType.EnemyE:
                    return new EnemyE(position);
                default:
                    return null;
            }
        }
    }
}


