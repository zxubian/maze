﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public enum ObjectType
    {
        Empty, Wall, Player, Goal, Item, 
        EnemyA, EnemyB, EnemyC, EnemyD, EnemyE
    };

    public enum Direction { Up, Down, Left, Right, Wait};
}
