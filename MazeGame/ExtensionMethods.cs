﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public static class ExtensionMethods
    {
        public static Vector2 ToVector2(this Direction dir)
        {
            Vector2 vec = new Vector2();
            switch (dir)
            {
                case Direction.Up:
                    vec = Vector2.Up;
                    break;
                case Direction.Down:
                    vec = Vector2.Down;
                    break;
                case Direction.Left:
                    vec = Vector2.Left;
                    break;
                case Direction.Right:
                    vec = Vector2.Right;
                    break;
            }
        return vec;
        }

        public static int Sign(this int i) {
            if (i > 0)
                return 1;
            else if (i < 0)
                return -1;
            else
                return 0;
        }

        public static Vector2 ToUnit(this Vector2 vec)
        {
            return new Vector2(vec.X.Sign(), vec.Y.Sign());
        }

        public static Direction ToDirection(this Vector2 vec)
        {
            if (vec.Y > 0)
                return Direction.Up;
            else if (vec.Y < 0)
                return Direction.Down;
            else if (vec.X > 0)
                return Direction.Right;
            else if (vec.X < 0)
                return Direction.Left;
            else
                return Direction.Wait;
        }

        public static Vector2 Add(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2 Opposite(this Vector2 vec)
        {
            return new Vector2(-vec.X, -vec.Y);
        }

        public static Vector2 Subtract(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        /// multiplies given vector by a scalar constant
        /// </summary>
        /// <param name="c">Constant to multiply by</param>
        public static Vector2 Times(this Vector2 vec, int c)
        {
            return new Vector2(vec.X * c, vec.Y * c);
        }
        public static void Swap<W>(this IList<W> list, int indexA, int indexB)
        {
            W tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }
    }


}

