﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Algorithms
    {

        public List<Tile> AStar(Tile source, Tile target)
            {
            BinaryHeap<Tile, float> frontier = new BinaryHeap<Tile, float>(BinaryHeap<Tile, float>.HeapType.MinHeap);
            frontier.Add(source, 0);
            Dictionary<Tile, Tile> comeFrom = new Dictionary<Tile, Tile>();
            comeFrom.Add(source, source);
            Dictionary<Tile, float> costSoFar = new Dictionary<Tile, float>();
            costSoFar.Add(source, 0);
            while (!frontier.isEmpty)
            {
                Tile current = frontier.ExtractTop();
                if (current.Equals(target))
                    break;
                foreach (Tile next in current.Neighbors)
                {
                    float newCost = costSoFar[current] + next.Weight;
                    if (!costSoFar.ContainsKey(next) || newCost < costSoFar[next])
                    {
                        costSoFar[next] = newCost;
                        float priority = newCost + Distance(source, target);
                        if (!float.IsInfinity(next.Weight))
                            frontier.Add(next, priority);
                        comeFrom.Add(next, current);
                    }
                }
            }
            return ReconstructPath(comeFrom, source, target);
        }

        List<Tile> ReconstructPath(Dictionary<Tile, Tile> cameFrom, Tile source, Tile target)
        {
            Tile current = target;
            List<Tile> path = new List<Tile>();
            path.Add(target);
            if (!cameFrom.ContainsKey(target))
            {
                path.Clear();
                path.Add(source);
                return path;
            }
            while (current != source)
            {
                current = cameFrom[current];
                path.Add(current);
            }
            path.Reverse();
            return path;
        }


        /// <summary>
        /// Returns the ManhattanDistance between 2 GameObjects
        /// </summary>
        public static int Distance(GameObject a, GameObject b)
        {
            return Distance(a.Position, b.Position);
        }

        /// <summary>
        /// Returns the ManhattanDistance between 2 tiles in the grid
        /// </summary>
        public static int Distance(Tile a, Tile b)
        {
            return Distance(a.Position, b.Position);
        }

        /// <summary>
        /// Returns the ManhattanDistance between 2 points in 2d space
        /// </summary>
        public static int Distance(Vector2 a, Vector2 b) {
            return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
        }
    }
}