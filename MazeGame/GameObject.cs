﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class GameObject
    {
        public string Name { get; private set; }
        public Vector2 Position { get; private set; }
        public ObjectType Type { get; private set; }

        public GameObject(string name, Vector2 position, ObjectType type) {
            Name = name;
            SetPosition(position);
            Type = type;
        }

        public void SetPosition(Vector2 newPosition)
        {
            Position = newPosition;
        }

        public override string ToString()
        { 
            return Name;
        }
    }
}
