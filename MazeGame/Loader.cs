﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

namespace MazeGame
{
    public class Loader
    {
        string[] MapText;
        public LevelData LoadLevelData(string path) {
            LoadMapFile(path);
            GameMap map = new GameMap(MapText);
            int startingTime = InterpretTime(path);
            return new LevelData(map,startingTime);
        }
        
        public void LoadMapFile(string path)
        {
            var lines = File.ReadLines(path).Skip(1);
            MapText = lines.ToArray();
        }

        /// <summary>
        /// Returns how much time is allocated for the given level.
        /// </summary>
        /// <returns>Time given at start of the level</returns>
        public int InterpretTime(string path)
        {
            int time = 0;
            string timeString = File.ReadLines(path).First();
            Regex timePattern = new Regex(@"(\d+)");
            Match timeMatch = timePattern.Match(timeString);
            time = Int32.Parse((timeMatch.Value));
            return time;
        }
    }
}
