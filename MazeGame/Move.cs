﻿namespace MazeGame
{
    public class Move
    {
        public Vector2 Start;
        public Vector2 End;
        public Vector2 LocalDirection;
        public Vector2 AbsoluteDirection;
        public GameObject MovingObject;
        public override string ToString()
        {
            return Start.ToString() + " -> " + End.ToString();
        }
    }

}
