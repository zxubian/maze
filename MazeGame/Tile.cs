﻿using System.Collections.Generic;

namespace MazeGame
{
    public class Tile
    {
        public float Weight{ get; private set; }
        public bool isWall{ get; private set; }
        public bool isEnemy{ get; private set; }
        public bool isPlayer{ get; private set; }
        public bool isItem{ get; private set; }
        public bool isGoal{ get; private set; }
        public List<Tile> Neighbors { get; private set; }
        public Vector2 Position;

        public Tile(Vector2 position)
        {
            Weight = 0f;
            isWall = isEnemy = isPlayer = isItem = isGoal = false;
            Neighbors = new List<Tile>();
            Position = position;
        }

        public void SetWeight(float weight) {
            Weight = weight;
        }

        public void ToggleFlags(ObjectType type, bool value)
        {
            switch (type)
            {
                case ObjectType.Empty:
                    break;
                case ObjectType.Wall:
                    isWall = value;
                    break;
                case ObjectType.Player:
                    isPlayer = value;
                    break;
                case ObjectType.Goal:
                    isGoal = value;
                    break;
                case ObjectType.Item:
                    isItem = value;
                    break;
                case ObjectType.EnemyA:
                case ObjectType.EnemyB:
                case ObjectType.EnemyC:
                case ObjectType.EnemyD:
                case ObjectType.EnemyE:
                    isEnemy = value;
                    break;
            }
        }


        public void ToggleFlags(ObjectType type) {
            switch (type)
            {
                case ObjectType.Empty:
                    break;
                case ObjectType.Wall:
                    isWall = !isWall;
                    break;
                case ObjectType.Player:
                    isPlayer = !isPlayer;
                    break;
                case ObjectType.Goal:
                    isGoal = !isGoal;
                    break;
                case ObjectType.Item:
                    isItem = !isItem;
                    break;
                case ObjectType.EnemyA:
                case ObjectType.EnemyB:
                case ObjectType.EnemyC:
                case ObjectType.EnemyD:
                case ObjectType.EnemyE:
                    isEnemy = !isEnemy;
                    break;
            }
        }

        public void ToggleFlags(GameObject gameObject) {
            ToggleFlags(gameObject.Type);
        }
        public override string ToString()
        {
            if (isEnemy)
                return "E";
            if (isPlayer)
                return "P";
            if (isItem)
                return "o";
            if (isGoal)
                return "G";
            if (isWall)
                return "#";
            else
                return " ";
        }
        public void AddNeighbor(Tile neighbor) {
            Neighbors.Add(neighbor);
        }
    }
}
        /*public Dictionary<Direction, Tile> TileNeighbors;
        new public void AddNeighbor(Node neighbor) { }
        public void AddNeighbor(Tile neighbor, Direction dir) {
            AdjacentNodes.Add(neighbor);
            TileNeighbors.Add(dir, neighbor);
        }
        public Tile(TileData data) : base(data.TileNeighbours.Values.ToArray(), data.Weight) { }
        */
  

    /*public class TileData {
        public Dictionary<Direction, Tile> TileNeighbours;
        public int Weight;
        public bool isWall;
        public bool Enemy;
        public bool Player;
        public bool Object;
        public bool Goal;

        public TileData() {
            TileNeighbours = new Dictionary<Direction, Tile>();
            Weight = 0;
            isWall = Enemy = Player = Object = Goal = false;
        }
    }
    */
