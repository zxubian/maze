﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Item: GameObject
    {
        public Item(Vector2 position) : base("o", position, ObjectType.Item) { }
    }
}
