﻿using System.Collections.Generic;

namespace MazeGame
{
    public class EnemyC : Roamer
    {

        public EnemyC(Vector2 position) : base("C", position, MovementSequences.SeqC, ObjectType.EnemyC) {}


        public override Vector2 NextDirection() {
            return Instructions[counter % 4];
        }
    }
}
