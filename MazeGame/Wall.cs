﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Wall : GameObject
    {
        public Wall(Vector2 position) : base("#", position,ObjectType.Wall) { }
    }
}
