﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class MovementSequences
    {
        public static List<Vector2> SeqA = new List<Vector2> { Vector2.Down, Vector2.Left, Vector2.Up, Vector2.Right };
        public static List<Vector2> SeqB = new List<Vector2> { Vector2.Up, Vector2.Left, Vector2.Down, Vector2.Right };
        public static List<Vector2> SeqC = new List<Vector2> { Vector2.Left, Vector2.Up, Vector2.Right, Vector2.Down };
        public static List<Vector2> SeqD = new List<Vector2> { Vector2.Right, Vector2.Up, Vector2.Left, Vector2.Down };
        public static List<Vector2> SeqE = new List<Vector2> { Vector2.Left, Vector2.Up, Vector2.Right, Vector2.Down, Vector2.Right, Vector2.Up, Vector2.Left, Vector2.Down };
    }
}
