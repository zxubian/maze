﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public abstract class Enemy : GameObject {
        protected Enemy(string name, Vector2 position, ObjectType type) : base(name, position, type) { }
        public abstract Move NextMove();
    }

}
