﻿using System.Collections.Generic;
namespace MazeGame
{
    public class EnemyD : Roamer
    {

        public EnemyD(Vector2 position) : base("D", position, MovementSequences.SeqD, ObjectType.EnemyD) { }


        public override Vector2 NextDirection()
        {
            return Instructions[counter % 4];
        }
    }
}
