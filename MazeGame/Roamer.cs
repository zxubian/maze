﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{

    /// <summary>
    /// Roamers are enemies that move in a pre-defined pattern, regardless of what the player is doing.
    /// Enemies C, D, and E follow this pattern
    /// </summary>
    public abstract class Roamer:Enemy
    {

        protected Vector2 facing;
        protected int counter;
        protected List<Vector2> Instructions;

        protected Roamer(string name, Vector2 position, List<Vector2> instructions, ObjectType type) : base(name, position, type)
        {
            Instructions = instructions;
            facing = Vector2.Up;
        }

        public override Move NextMove() {
            Move nextMove = new Move();
            nextMove.Start = Position; //start where we are
            nextMove.LocalDirection = NextDirection(); //get next position in list
            facing = Vector2.TurnToDirection(facing, nextMove.LocalDirection); //change facing direction
            nextMove.End = Vector2.Add(nextMove.Start, facing); //take a step in the new direction
            nextMove.AbsoluteDirection = facing;
            nextMove.MovingObject = this;
            counter++;
            return nextMove;
        }

        public abstract Vector2 NextDirection();
    }
}
