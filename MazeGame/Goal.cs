﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Goal:GameObject
    {
        public Goal(Vector2 position) : base("G", position,ObjectType.Goal) { }
    }
}
