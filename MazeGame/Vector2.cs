﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Vector2
    {

        public int X { get; private set; }
        public int Y { get; private set; }

        public Vector2(int x, int y) {
            X = x;
            Y = y;
        }

        public Vector2()
        {
            X = 0;
            Y = 0;
        }

        public static Vector2 Add(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        /// Returns unit vector in given direction, with given vector as base
        /// </summary>
        /// <returns></returns>
        public static Vector2 TurnToDirection(Vector2 Facing, Vector2 dir){
            Direction direction = dir.ToDirection();
            Vector2 directionVector = new Vector2();
            switch (direction) {
                case Direction.Up:
                    directionVector = Facing;
                    break;
                case Direction.Down:
                    directionVector = Facing.Opposite();
                    break;
                case Direction.Left:
                    directionVector.X = -Facing.Y;
                    directionVector.Y = Facing.X;
                    break;
                case Direction.Right:
                    directionVector.X = Facing.Y;
                    directionVector.Y = -Facing.X;
                    break;
            }         
            return directionVector.ToUnit();
        }


        public override string ToString() {
            string vectorString = string.Format ("({0},{1})", X.ToString(), Y.ToString());
            return vectorString;
        }


        public static Vector2 Up { get { return new Vector2(0, 1); } }
        public static Vector2 Down { get { return new Vector2(0, -1); } }
        public static Vector2 Left { get { return new Vector2(-1, 0); } }
        public static Vector2 Right { get { return new Vector2(1, 0); } }


    }
    
}
