﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{

    /// <summary>
    /// The Chaser is an enemy that follows the player
    /// </summary>
    public abstract class Chaser : Enemy
    {

        public List<Vector2> Instructions;
        public Vector2 DeltaPos { get; private set; }

        protected Chaser(string name, Vector2 position, List<Vector2> instructions, ObjectType type) : base(name, position, type)
        {
            Instructions = instructions;
        }


       

        public int ShouldMoveY() {
            if (DeltaPos.Y != 0) {
                int yMovement = DeltaPos.Y.Sign();
                Vector2 dir;
                if (DeltaPos.Y > 0)
                    dir = Vector2.Up;
                else
                    dir = Vector2.Down;
                if (CanMoveInDirection(dir)) {
                    return yMovement;
                }
                else return 0;
            }
            else return 0;
        }



        public int ShouldMoveX()
        {
            if (DeltaPos.X != 0)
            {
                int xMovement = DeltaPos.X.Sign();
                Vector2 dir;
                if (DeltaPos.X > 0)
                    dir = Vector2.Right;
                else
                    dir = Vector2.Left;
                if (CanMoveInDirection(dir))
                {
                    return xMovement;
                }
                else return 0;
            }
            else return 0;
        }

        public Vector2 FirstPossibleDirection() {
            foreach (Vector2 direction in Instructions) {
                if (CanMoveInDirection(direction)) {
                    return direction;
                }
            }
            return new Vector2();
        }

        public bool CanMoveInDirection(Vector2 direction) {
            return GameMap.Instance.TileUnoccupied(Vector2.Add(Position, direction));
        }


        public void UpdateDeltaPos() {
            DeltaPos = Player.instance.Position.Subtract(Position);
        }

        

    }
}
