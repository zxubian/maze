﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MazeGame
{
    class GameSimulator
    {
        public int CurrentTime { get; private set; }
        public void SetTime(int value) {
            CurrentTime = value;
        }
        public Player PlayerObj { get; private set; }
        public List<GameObject> Enemies { get { return GameMap.Instance.Enemies; } }
        public int itemCount;
        public enum GameState { playing, win, loose}
        public GameState CurrentState; 
        public string PlayerMoves = "";
        public GameMap gameMap;
        public static GameSimulator Instance;
        Move playerMove;
        List<Move> enemyMoves = new List<Move>();
        public void StartSimulation(LevelData data) {
            Instance = this;
            CurrentState = GameState.playing;
            SetTime(data.Time);
            gameMap = data.Map;
            PlayerObj = gameMap.PlayerObj;
            itemCount = gameMap.Items.Count;
            GameLoop();
        }


        public void GameLoop() {
            while (CurrentTime != 0) {
                DoPlayerMove();
                if (CurrentState != GameState.win)
                {
                    DoEnemyMoves();
                    if (CurrentState != GameState.loose)
                    {
                        --CurrentTime;
                        RefreshConsole();
                        Console.ReadKey();
                    }
                    else
                    {
                        FinishGame();
                    }
                }
                else {
                    FinishGame();
                    return;
                }
            }
            CurrentState = GameState.loose;
            FinishGame();
        }

        public void RefreshConsole() {
            Console.Clear();
            PrintTime();
            GameMap.Instance.PrintGameMap();
            Console.WriteLine("Press any key to go to next frame");
        }

        public void DoPlayerMove() {
            playerMove = PlayerObj.NextMove();
            GameMap.Instance.MoveObject(playerMove);
            DealWithItem(playerMove);
            DealWithGoal();
            AppendPlayerMove(playerMove);
        }

        public void DealWithGoal() {
            if (itemCount == 0) {
                if (GameMap.Instance.PlayerOnGoal())
                    StopGameLoop(GameState.win);
            }
        }
        public void DealWithItem(Move move) {
            if (GameMap.Instance.CanTakeItem()) {
                GameMap.Instance.PlayerTakeItem();
                --itemCount;
            }
        }

        public void DoEnemyMoves() {
            enemyMoves.Clear();
            foreach (Enemy enemy in Enemies)
            {
                Move EnemyMove = enemy.NextMove();
                GameMap.Instance.MoveObject(EnemyMove);
                if (GameMap.Instance.PlayerDead())
                {
                    StopGameLoop(GameState.loose);
                    break;
                }
            }
        }

        public void StopGameLoop(GameState state) {
            CurrentState = state;
            CurrentTime = 0;
        }

        public void FinishGame() {
            int score = CurrentTime;
            Console.Clear();
            if (CurrentState == GameState.win)
                Console.WriteLine("You Won!");
            else
                Console.WriteLine("You Lost!");
            GameMap.Instance.PrintGameMap();
            Console.WriteLine("Score: {0}", score);
            PrintPlayerMoves();
        }

        public void MoveObject(ref GameObject gameObject, Vector2 position, ref GameMap map) {
            //get old position && process what was there
            gameObject.SetPosition(position);
            map.PlaceGameObject(ref gameObject, position);
            //set old position to null or maybe just the previous state?
        }

        public void AppendPlayerMove(Move move) {
            PlayerMoves += move.AbsoluteDirection.ToDirection().ToString().First();
        }

        public void PrintTime() {
            Console.WriteLine("Time: {0}", CurrentTime);
        }

        public void PrintPlayerMoves() {
            Console.WriteLine("Player Moves: {0}", PlayerMoves);
            File.WriteAllText(Directory.GetCurrentDirectory() + @"\playerMoves.txt", PlayerMoves.ToLower());
        }
    }
}
