﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class Player : GameObject
    {
        public Item ClosestItem;
        public Enemy ClosestEnemy;
        public BinaryHeap<List<Tile>, int> ItemPaths;
        public BinaryHeap<Enemy, int> EnemyDistances;
        Algorithms pathfinder;

        public Player(Vector2 position) : base("P", position,ObjectType.Player) {
            instance = this;
            pathfinder = new Algorithms();
        }


        public Move NextMove()
        {
            Tile TargetTile = GetNewTarget();
            Move move = new Move();
            move.Start = Position;
            move.End = TargetTile.Position;
            move.AbsoluteDirection = move.Start.Subtract(move.End).ToUnit();
            move.LocalDirection = move.AbsoluteDirection;
            move.MovingObject = this;
            return move;
        }

        public Tile GetNewTarget() {
            GameMap.Instance.UpdateWeights(true);
            if (GameSimulator.Instance.itemCount > 0)
            {
                return GetClosestItemPath()[1];
            }
            else
                return GetGoalPath()[1];
        }

        public List<Tile> GetGoalPath() {
            Tile playerTile = GameMap.Instance.FetchTile(Player.instance.Position);
            Tile goalTile = GameMap.Instance.FetchTile(GameMap.Instance.Goal.Position);
            List<Tile> GoalPath = pathfinder.AStar(playerTile, goalTile);
            if (GoalPath.Count == 1)
                GoalPath.Add(playerTile);
            return GoalPath;
        }

        /// <summary>
        /// We will generate full paths for the 3 closest items and pick the shortest one.
        /// </summary>
        /// <returns></returns>
        public List<Tile> GetClosestItemPath() {
            Tile playerTile = GameMap.Instance.FetchTile(Player.instance.Position);
            ItemPaths = new BinaryHeap<List<Tile>, int>(BinaryHeap<List<Tile>, int>.HeapType.MinHeap);
            BinaryHeap<Tile, int> ItemDistances = new BinaryHeap<Tile, int>(BinaryHeap<Tile, int>.HeapType.MinHeap);
            foreach (Tile itemTile in GameMap.Instance.ItemTiles.Values)
            {
                ItemDistances.Add(itemTile, DistanceToPlayer(itemTile));
            }
            int checkItems = GameSimulator.Instance.itemCount > 3 ? 3 : GameSimulator.Instance.itemCount;
            for (int i = 0; i != checkItems; ++i) {
                List<Tile> path = pathfinder.AStar(playerTile,ItemDistances.ExtractTop());
                if (path.Count > 1)
                    ItemPaths.Add(path, path.Count);
                else if(path[0]!=playerTile)
                    ItemPaths.Add(path, path.Count);
            }
            if(!ItemPaths.isEmpty)
                return ItemPaths.PeekTop();
            else
                return new List < Tile > { playerTile, playerTile };
        }


        public Enemy GetClosestEnemy() {
            EnemyDistances = new BinaryHeap<Enemy, int>(BinaryHeap<Enemy, int>.HeapType.MinHeap);
            foreach (Enemy enemy in GameMap.Instance.Enemies)
                EnemyDistances.Add(enemy, DistanceToPlayer(enemy));
            return EnemyDistances.PeekTop();
        }

        /// <summary>
        /// Returns the Manhattan distance to the player from a given tile
        /// </summary>
        public int DistanceToPlayer(Tile tile) {
            return Algorithms.Distance(tile.Position, Player.instance.Position);
        }

        public int DistanceToPlayer(GameObject gameObject)
        {
            return Algorithms.Distance(gameObject, Player.instance);
        }


        public Move TryMoveRight() {
            Move nextMove = new Move();
            Vector2 endVector = Vector2.Add(Position, Vector2.Right);
            if (GameMap.Instance.TileUnoccupied(endVector))
            {
                nextMove.AbsoluteDirection = Vector2.Right;
                nextMove.LocalDirection = Vector2.Right;
                nextMove.Start = Position;
                nextMove.End = endVector;
                nextMove.MovingObject = this;
            }
            else
            {
                nextMove.AbsoluteDirection = nextMove.LocalDirection = new Vector2();
                nextMove.Start = nextMove.End = Position;
                nextMove.MovingObject = this; 
            }
            return nextMove;
        }
        public static Player instance;
    }
}
